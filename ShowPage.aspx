﻿<%@ Page Language="C#" Inherits="final_project.ShowPage" AutoEventWireup="true" CodeFile="ShowPage.aspx.cs" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="container">

    <asp:SqlDataSource runat="server" id="page_select"
    ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
    
    <asp:SqlDataSource runat="server" id="page_delete"
    ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
   
    
    <h3 runat="server" id="page_title"></h3>
    <p id="page_content" runat="server"></p>
    
    
    <a href="EditPage.aspx?pageid=<%Response.Write(this.pageid);%>">Edit</a>
    <asp:Button runat="server" id="delete_btn" Text="Delete Page" OnClick="Delete_Page"></asp:Button>
    <div id="debug" runat="server"></div>
    
    
</asp:Content>