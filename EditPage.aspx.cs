﻿using System;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace final_project
{

    public partial class EditPage : System.Web.UI.Page
    {

        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        protected void Edit_Page(object sender, EventArgs e)
        {

            //string title = page_title.Text;
            //string content = page_content.Text;
            //string query = "update into pages (pagetitle, pagecontent) values ('" + title + "', '" + content + "')";

        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            string query = "select * from pages where pageid ="
            + pageid;

            edit_page.SelectCommand = query;

            DataView pageview = (DataView)edit_page.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview = pageview[0];

            page_content.Text = pagerowview["pagecontent"].ToString();
            page_title.Text = pagerowview["pagetitle"].ToString();

        }

        protected void Update_Page(object sender, EventArgs e)
        {
            string title = page_title.Text;
            string content = page_content.Text;

            //string query = "UPDATE pages set pagetitle = '" + title + "', '"pagecontent = "' + content + "' WHERE pageid=" + pageid;


            string query = "update pages set pagetitle='" + title + "', pagecontent='" + content + "' where pageid=" + pageid;

            edit_page.UpdateCommand = query;
            edit_page.Update();
            
        }
    }
}