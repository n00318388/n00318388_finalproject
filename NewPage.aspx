﻿<%@ Page Language="C#" Inherits="final_project.NewPage" AutoEventWireup="true" CodeFile="NewPage.aspx.cs" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="container">

	<asp:SqlDataSource id="new_page" runat="server"  ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
        
        
            <h3>Create New Page</h3>
                <label>Page Title</label>
                <asp:TextBox runat="server" id="page_title" />
        <br />
        <br />
                <label>Page Content</label>
                <asp:TextBox runat="server" id="page_content" />
        <br />
        <br />
                <asp:Button Text="Save" runat="server" OnClick="New_Page"/>
	    
            <div id="querychk" runat="server"></div>
        
</asp:Content>