﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;

namespace final_project
{

    public partial class Menu : System.Web.UI.UserControl
    {
        protected void Page_Load(object Sender, EventArgs e)
        {
            //write a query that finds all the pages
            //create a result set that we can iterate through
            //foreach page in the pages table, create
            //<li><a href="ShowPage.aspx?pageid=id">title</a></li>
            string query = "select * from pages";
            pages_select.SelectCommand = query;

            DataView pageview = (DataView)pages_select.Select(DataSourceSelectArguments.Empty);

            string menustr = "<li><a href=\"Manage_Pages.aspx\">Home</a></li>";
            
            foreach (DataRowView row in pageview)
            {
                string id = row["pageid"].ToString();
                string title = row["pagetitle"].ToString();
                menustr += "<li><a href=\"ShowPage.aspx?pageid="+id+"\">"+title+"</a></li>";
            }

            menucontainer.InnerHtml = menustr;
        }
    }
}
