﻿using System;
using System.Web;
using System.Web.UI;
using System.Data;

namespace final_project
{
    public partial class ShowPage : System.Web.UI.Page
    {
        public string pageid
        {
            get{ return Request.QueryString["pageid"]; }
        }

        protected void Page_Load(object Sender, EventArgs e)
        {

            DataRowView pageview = GetPageInfo(pageid);
            string pagetitle = pageview["pagetitle"].ToString();
            string pagecontent = pageview["pagecontent"].ToString();

            page_title.InnerHtml = pagetitle;
            page_content.InnerHtml = pagecontent;
            
        }

        //get page info will return information about the page given an id.
        protected DataRowView GetPageInfo(string id)
        {
            string query = "select * from pages where pageid=" + id;
            page_select.SelectCommand = query;
            
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }

        protected void Delete_Page(object Sender, EventArgs e)
        {
            string delquery = "delete from pages where pageid="+pageid;
            //debug.InnerHtml = delquery;

            page_delete.DeleteCommand = delquery;
            page_delete.Delete();
        }
        
    }
}
