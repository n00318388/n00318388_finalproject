﻿<%@ Page Language="C#" Inherits="final_project.Manage_Pages" AutoEventWireup="true" CodeFile="Manage_Pages.aspx.cs" MasterPageFile="~/Site.Master" %>
<asp:Content runat="server" ContentPlaceHolderID="container">
    <h1>Manage Pages</h1>
    
        <div>
        <asp:Button runat="server"  class="mybtn" Text="Add New Page" OnClick="New_Page" />
        <br /><br />
        <asp:SqlDataSource id="pages_select" runat="server"  ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
        
        <asp:DataGrid id="pages_list" runat="server"></asp:DataGrid>
    </div>
</asp:Content>
