﻿<%@ Page Language="C#" Inherits="final_project.EditPage" AutoEventWireup="true" CodeFile="EditPage.aspx.cs" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="container">

    <asp:SqlDataSource runat="server" id="edit_page"
    ConnectionString="<%$ ConnectionStrings:final_sql_con %>"></asp:SqlDataSource>
        
    <h3>Edit Page</h3>
        <label>Page Title</label>
        <asp:TextBox runat="server" id="page_title" />
        <br />
        <br />
        <label>Page Content</label>
        <asp:TextBox runat="server" id="page_content" />
        <br />
        <br />
        <asp:Button Text="Save" runat="server" OnClick="Update_Page"/>

</asp:Content>