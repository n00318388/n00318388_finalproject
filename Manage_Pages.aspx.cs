﻿using System;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace final_project
{

    public partial class Manage_Pages : System.Web.UI.Page
    {
        public string sqlquery = "select * from pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = sqlquery;
            pages_list.DataSource = pages_manual_bind(pages_select);
            pages_list.DataBind();
        }

        protected DataView pages_manual_bind(SqlDataSource source)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)source.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["pagetitle"] =
                    "<a href=\"ShowPage.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";

            }
            mytbl.Columns["pagetitle"].ColumnName = "Title";
            mytbl.Columns["pagecontent"].ColumnName = "Content";
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
        }

        protected void New_Page(object sender, EventArgs e)
        {
            Response.Redirect("NewPage.aspx", false);
        }
    }
}